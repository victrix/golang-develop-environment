package main

import (
	"api/src/controllers"
	"api/src/models"
	"log"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load()

	if err != nil {
		log.Fatal("Loading .env failed.")
	}

	appPort := os.Getenv("APP_PORT")

	r := gin.Default()

	models.DBConnect()

	r.GET("/", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "OK",
		})
	})

	r.GET("/users", controllers.Find)
	r.GET("/users/:id", controllers.FindOne)
	r.POST("/users", controllers.Create)
	r.PUT("/users/:id", controllers.Update)
	r.DELETE("/users/:id", controllers.Delete)

	r.Run(":" + appPort)
}
