package validators

type UserCreateInput struct {
	Name string `json:"name" binding:"required"`
	Age  int64  `json:"age" binding:"required"`
}

type UserUpdateInput struct {
	Name string `json:"name" binding:"required"`
	Age  int64  `json:"age"`
}
