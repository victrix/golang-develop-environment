package models

import (
	"time"

	"gorm.io/gorm"
)

type User struct {
	// gorm.Model
	ID       uint      `json:"id" gorm:"primaryKey"`
	Name     string    `json:"name"`
	Age      int64     `json:"age"`
	Products []Product // Usable
	// Products []Product `gorm:"foreignkey:UserID"` // Usable
	// Products  []Product `gorm:"column:product_id"` // Usable
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt `gorm:"index"`
}
