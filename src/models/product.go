package models

import (
	"time"

	"gorm.io/gorm"
)

type Product struct {
	// gorm.Model
	ID        uint   `json:"id" gorm:"primaryKey"`
	Name      string `json:"name"`
	Price     int64  `json:"price"`
	UserID    uint
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt `gorm:"index"`
}
