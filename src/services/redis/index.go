package redis

import (
	"api/src/models"
	"context"
	"encoding/json"
	"time"

	"github.com/go-redis/redis/v8"
)

var ctx = context.Background()

var rdb = redis.NewClient(&redis.Options{
	Addr:     "redis:6379", // Map with network alias
	Password: "",
	DB:       0,
})

func Get(key string) interface{} {
	val, err := rdb.Get(ctx, key).Result()

	if err != nil {
		// panic(err)
	}

	var users []models.User

	json.Unmarshal([]byte(val), &users)

	return &users
}

func Set(key string, value interface{}) bool {
	js, _ := json.Marshal(value)

	set, err := rdb.SetNX(ctx, key, js, 10*time.Second).Result()

	if err != nil {
		panic(err)
	}

	return set
}
