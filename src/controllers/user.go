package controllers

import (
	"api/src/models"
	"api/src/services/redis"
	"api/src/validators"
	"net/http"

	"github.com/gin-gonic/gin"
)

func Find(c *gin.Context) {
	result := redis.Get("key")

	var users []models.User

	models.DB.Find(&users)

	redis.Set("key", &users)

	result = &users

	c.JSON(200, gin.H{
		"message": "OK",
		"data":    result,
	})
}

func FindOne(c *gin.Context) {
	id := c.Param("id")

	user := models.User{}

	models.DB.Preload("Products").First(&user, id)

	c.JSON(200, gin.H{
		"message": "OK",
		"data":    user,
	})
}

func Create(c *gin.Context) {
	var input validators.UserCreateInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})

		return
	}

	user := models.User{Name: input.Name, Age: input.Age}
	models.DB.Create(&user)

	c.JSON(200, gin.H{
		"message": "OK",
		"data":    true,
	})
}

func Update(c *gin.Context) {
	id := c.Param("id")

	user := models.User{}
	models.DB.First(&user, id)

	if err := models.DB.Where("id = ?", c.Param("id")).First(&user).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})

		return
	}

	var input validators.UserUpdateInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()}) // 400 Bad Request

		return
	}

	payload := models.User{Name: input.Name, Age: input.Age}

	models.DB.Model(&user).Updates(payload)

	c.JSON(200, gin.H{
		"message": "OK",
		"data":    user,
	})
}

func Delete(c *gin.Context) {
	var user models.User

	if err := models.DB.Where("id = ?", c.Param("id")).First(&user).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})

		return
	}

	// Soft delete
	models.DB.Delete(&user)

	c.JSON(200, gin.H{
		"message": "OK",
		"data":    true,
	})
}
