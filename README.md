# environments
1. golang v1.16
2. redis v6
3. mysql v8

### dependencies
go get github.com/joho/godotenv
go get github.com/gin-gonic/gin
go get gorm.io/gorm
go get gorm.io/driver/mysql
go get github.com/go-redis/redis/v8

### dev dependencies
go get github.com/codegangsta/gin (Live reload)
gin --appPort 8000 --port 8080

### Need to upgrading to 1.18 Try to use go install instead go get